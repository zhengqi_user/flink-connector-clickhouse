package site.gaoxiaoming.table;

import org.apache.flink.api.common.serialization.SerializationSchema;
import org.apache.flink.connector.jdbc.internal.options.JdbcOptions;
import org.apache.flink.table.connector.ChangelogMode;
import org.apache.flink.table.connector.format.EncodingFormat;
import org.apache.flink.table.connector.sink.DynamicTableSink;
import org.apache.flink.table.connector.sink.SinkFunctionProvider;
import org.apache.flink.table.data.RowData;
import org.apache.flink.table.types.DataType;

/**
 * @author lucas
 */
public class ClickHouseDynamicTableSink implements DynamicTableSink {

    private final JdbcOptions jdbcOptions;
    private final EncodingFormat<SerializationSchema<RowData>> encodingFormat;
    private final DataType dataType;

    private final String localTableName;
    private final String primaryKey;

    public ClickHouseDynamicTableSink(JdbcOptions jdbcOptions,
                                      EncodingFormat<SerializationSchema<RowData>> encodingFormat,
                                      DataType dataType,
                                      String localTableName,
                                      String primaryKey
    ) {
        this.jdbcOptions = jdbcOptions;
        this.encodingFormat = encodingFormat;
        this.dataType = dataType;
        this.localTableName = localTableName;
        this.primaryKey = primaryKey;
    }

    @Override
    public ChangelogMode getChangelogMode(ChangelogMode requestedMode) {
        return requestedMode;
    }

    @Override
    public SinkRuntimeProvider getSinkRuntimeProvider(Context context) {
        SerializationSchema<RowData> serializationSchema = encodingFormat.createRuntimeEncoder(context, dataType);
        ClickHouseSinkFunction clickHouseSinkFunction = new
                ClickHouseSinkFunction(jdbcOptions, serializationSchema, localTableName, primaryKey);
        return SinkFunctionProvider.of(clickHouseSinkFunction);
    }

    @Override
    public DynamicTableSink copy() {

        return new ClickHouseDynamicTableSink(jdbcOptions, encodingFormat,
                dataType, localTableName, primaryKey);
    }

    @Override
    public String asSummaryString() {
        return "ClickHouse Table Sink";
    }

}
