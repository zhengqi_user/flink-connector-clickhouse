package site.gaoxiaoming.table;

import org.apache.commons.lang3.StringUtils;
import org.apache.flink.api.common.serialization.SerializationSchema;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.connector.jdbc.internal.options.JdbcOptions;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.apache.flink.table.data.RowData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.yandex.clickhouse.BalancedClickhouseDataSource;
import ru.yandex.clickhouse.ClickHouseConnection;
import ru.yandex.clickhouse.ClickHouseDataSource;
import ru.yandex.clickhouse.ClickHouseStatement;
import ru.yandex.clickhouse.domain.ClickHouseFormat;
import ru.yandex.clickhouse.settings.ClickHouseProperties;
import ru.yandex.clickhouse.settings.ClickHouseQueryParam;
import site.gaoxiaoming.utils.JsonUtils;

import java.io.ByteArrayInputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author lucas
 */
public class ClickHouseSinkFunction extends RichSinkFunction<RowData> {

    private static final long serialVersionUID = 1L;

    private static final String MAX_PARALLEL_REPLICAS_VALUE = "2";

    private final JdbcOptions jdbcOptions;
    private final SerializationSchema<RowData> serializationSchema;
    private static Logger logger = LoggerFactory.getLogger(ClickHouseSinkFunction.class);
    private ClickHouseConnection conn;
    private ClickHouseStatement stmt;

    private String localTableName;
    private String primaryKey;

    private final String DELETE_TEMPLATE = "ALTER TABLE %s" + " " + "on cluster default "
            + "  delete  where ";
    private String deleteSql = null;
    private Buffer buffer = null;
    private List<Map<String, RowData>> list = null;

    private ScheduledExecutorService service = null;


    public ClickHouseSinkFunction(JdbcOptions jdbcOptions,
                                  SerializationSchema<RowData>
                                          serializationSchema,
                                  String localTableName,
                                  String primaryKey
    ) {
        this.jdbcOptions = jdbcOptions;
        this.serializationSchema = serializationSchema;
        this.localTableName = localTableName;
        this.primaryKey = primaryKey;
    }

    @Override
    public void open(Configuration parameters) {
        ClickHouseProperties properties = new ClickHouseProperties();
        properties.setUser(jdbcOptions.getUsername().orElse(null));
        properties.setPassword(jdbcOptions.getPassword().orElse(null));
        BalancedClickhouseDataSource dataSource;
        service = Executors.newSingleThreadScheduledExecutor();
        list = new ArrayList<>();
        service.scheduleAtFixedRate(() -> {
            if (buffer != null) {
                synchronized (buffer) {
                    if (buffer.canFlush()) {
                        list.add(buffer.getMap());
                        buffer.unset();
                    }
                }
                for (Map<String, RowData> map : list) {
                    for (Map.Entry<String, RowData> entry : map.entrySet()) {
                        try {
                            commitUpdateRecord(entry.getValue());
                        } catch (Exception e) {
                            logger.error("commit to clickhouse failed,caused by:", e);
                        }
                    }
                }
                list = new ArrayList<>();
            }
        }, 0, 1l, TimeUnit.SECONDS);

        try {
            if (StringUtils.isNotEmpty(primaryKey)) {
                deleteSql = String.format(DELETE_TEMPLATE, localTableName);
            }

            if (null == conn) {
                dataSource = new BalancedClickhouseDataSource(jdbcOptions.getDbURL(), properties);
                conn = dataSource.getConnection();
            }
        } catch (SQLException e) {
            throw new RuntimeException("create connection failed,caused by", e);
        }
    }

    @Override
    public void invoke(RowData value, Context context) throws Exception {
        if (StringUtils.isEmpty(deleteSql)) {
            commitUpdateRecord(value);
        } else {
            Map<String, Object> map = JsonUtils.byte2map(serializationSchema.serialize(value));
            String key = getKey(map);
            if (buffer == null) {
                buffer = new Buffer();
            }
            synchronized (buffer) {
                buffer.addBuffer(key, value, serializationSchema.serialize(value).length);
            }
        }
    }

    private String getKey(Map<String, Object> map) {
        StringBuilder sb = new StringBuilder();
        for (String field : primaryKey.split(",")) {
            sb.append(map.get(field)).append("-");
        }
        return sb.toString().substring(0, sb.toString().lastIndexOf("-"));
    }


    private void commitUpdateRecord(RowData value) throws Exception {
        byte[] serialize = serializationSchema.serialize(value);
        stmt = conn.createStatement();
        if (StringUtils.isNotEmpty(deleteSql)) {
            Map<String, Object> map = JsonUtils.byte2map(serialize);
            StringBuilder condition = new StringBuilder();
            for (String key : primaryKey.split(",")) {
                condition.append(key).append("=").append("'").append(map.get(key)).append("'")
                        .append("  ").append(" and  ");
            }
            String conditionStr = condition.toString();
            String deleteData = deleteSql + conditionStr.substring(0, conditionStr.lastIndexOf("and"));
            int code = stmt.executeUpdate(deleteData);
//            logger.info("execute sql:" + deleteData + " successfully with code:" + code);
        }
        stmt.write().table(jdbcOptions.getTableName()).data(new ByteArrayInputStream(serialize), ClickHouseFormat.JSONEachRow)
                .addDbParam(ClickHouseQueryParam.MAX_PARALLEL_REPLICAS, MAX_PARALLEL_REPLICAS_VALUE).send();
    }

    @Override
    public void close() throws Exception {
        service.shutdown();
        service.awaitTermination(1, TimeUnit.MINUTES);
        if (buffer != null) {
            for (Map.Entry<String, RowData> entry : buffer.getMap().entrySet()) {
                commitUpdateRecord(entry.getValue());
            }
        }
        if (stmt != null) {
            stmt.close();
        }
        if (conn != null) {
            conn.close();
        }
    }

}
