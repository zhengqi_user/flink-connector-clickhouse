package site.gaoxiaoming.table;

import org.apache.flink.table.data.RowData;

import java.util.HashMap;
import java.util.Map;

/**
 * thread unsafe
 */
public class Buffer {
    private Map<String, RowData> map = new HashMap<>();
    private long time;
    private long size = 0;
    private static final int FIVE_MB = 1024 * 1024 * 1024 * 5;

    public Buffer() {
        this.time = System.currentTimeMillis();
    }

    public void addBuffer(String key, RowData data, int size) {
        map.put(key, data);
        this.size += size;
    }


    public Map<String, RowData> getMap() {
        return map;
    }

    public void unset() {
        map = new HashMap<>();
        this.time = System.currentTimeMillis();
        this.size = 0;
    }

    public boolean canFlush() {
        return size >= FIVE_MB || (System.currentTimeMillis() - time) / 1000 >= 3;
    }

}
