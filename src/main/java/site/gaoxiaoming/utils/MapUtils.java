package site.gaoxiaoming.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.Map;

public class MapUtils {
    public static <T> T getValueIgnoreCase(String key, Map<String, T> map) {
        if (map == null || StringUtils.isEmpty(key)) {
            return null;
        }

        for (Map.Entry<String, T> entry : map.entrySet()) {
            if (key.equalsIgnoreCase(entry.getKey())) {
                return entry.getValue();
            }
        }
        return null;
    }
}
