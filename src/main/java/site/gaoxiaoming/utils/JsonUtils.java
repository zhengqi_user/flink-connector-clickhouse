package site.gaoxiaoming.utils;

import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Map;

public class JsonUtils {

    public static Map<String, Object> byte2map(byte[] bytes) throws JsonProcessingException {
        String json = new String(bytes);
        return new ObjectMapper().reader(Map.class).readValue(json);
    }
}
