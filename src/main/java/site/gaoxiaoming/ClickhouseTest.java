package site.gaoxiaoming;

import ru.yandex.clickhouse.BalancedClickhouseDataSource;
import ru.yandex.clickhouse.ClickHouseConnection;
import ru.yandex.clickhouse.ClickHouseStatement;
import ru.yandex.clickhouse.domain.ClickHouseFormat;
import ru.yandex.clickhouse.settings.ClickHouseProperties;
import ru.yandex.clickhouse.settings.ClickHouseQueryParam;

import java.io.ByteArrayInputStream;
import java.sql.SQLException;

public class ClickhouseTest {

    public static void main(String[] args) {
        ClickHouseProperties properties = new ClickHouseProperties();
        properties.setUser("meb_rt_dw");
        properties.setPassword("Meb2020&!Ck123%");
        BalancedClickhouseDataSource dataSource;
        ClickHouseConnection conn;
        String content = "{\"Email\":null,\"IsActive\":1,\"IsForbidden\":0,\"Birthday\":null,\"JunGender\":null,\"TenantUserDresserTag\":null,\"Salt\":null,\"TenantUserId\":\"4c7fe8bf-3ca0-4934-8ca1-252864443797\",\"ClientId\":\"4c7fe8bf-3ca0-4934-8ca1-252864443797\",\"IDNumber\":null,\"Password\":null,\"UserName\":\"4c7fe8bf-3ca0-4934-8ca1-252864443797\",\"TenantUserRoleType\":null,\"TenantName\":\"MEB\",\"DeptPath\":null,\"HospitalId\":null,\"CreateTime\":null,\"JunNickName\":null,\"Avatar\":\"https://cdn-ssl.meb.com/photo-s\",\"TenantUserType\":\"\",\"TenantId\":1,\"UserId\":4014049,\"UpdateTime\":null,\"JunPhoneNumber\":null,\"DepartmentId\":0,\"RealName\":\"顾客4014049\",\"Tag\":null,\"UserType\":2,\"ChannelType\":4,\"age\":null}";
        String content1 = "{\"Email\":null,\"IsActive\":1}";
        try {
            String url = "jdbc:clickhouse://120.26.67.183:8436/warehouse";
            dataSource = new BalancedClickhouseDataSource(url, properties);
            conn = dataSource.getConnection();
            ClickHouseStatement stmt = conn.createStatement();
            stmt.write().table("ods_user_test").data(new ByteArrayInputStream(content.getBytes()), ClickHouseFormat.JSONEachRow)
                    .addDbParam(ClickHouseQueryParam.MAX_PARALLEL_REPLICAS, "2").send();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
